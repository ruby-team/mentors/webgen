Webgen is dynamic and some extra features can be activated by installing the
following packages:

- ruby-redcloth if you want Textile support. (http://hobix.com/textile/)
- ruby-rmagick if you want automatic thumbnail creation for picture
  galleries.
- libexif-ruby if you want to be able to have EXIF
  information available for image galleries.
- ruby-kramdown if you want Markdown support.
- ruby-builder if you want to be able to programmatically create XHTML/XML
  documents.

================================================================================

Use the command "webgen help" to see webgen's inline help.

More Information you an get here:
* The webgen User Documentation at <http://webgen.gettalong.org/documentation/>
* The mailing list archive at 
  <https://groups.google.com/forum/?fromgroups#!forum/webgen-users>
* The webgen Wiki at <http://github.com/gettalong/webgen/wiki>

Have a look at <http://webgen.gettalong.org/news.html> for a list of changes!

================================================================================

Quickstart tour:

- First type: webgen create sample_site
  A directory sample_site is created containing a webgen site template.

- Go in the newly created directory and type webgen (or webgen run) to generate
  the html pages from the templates.

